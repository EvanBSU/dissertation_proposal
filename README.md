# README #

This repo contains Evan Miller's dissertation proposal. 
It is written in LATEX.
In order for the document to compile, the cmelab.bib file needs to be in the directory with the makefile. 
Once you get that set up. Use make to create the document and enjoy!